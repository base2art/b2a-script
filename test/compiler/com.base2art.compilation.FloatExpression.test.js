describe("Floating point Expression Compiler", function() {
  var compiler;
  var lexer;
  var insts = com.base2art.compilation.instructionSet;
  
  beforeEach(function() {
    lexer = new com.base2art.compilation.Lexer();
    compiler = new com.base2art.compilation.JavaScriptCompiler();
  });

  it("Can Compile '3'", function() {
    expect(eval(compiler.compile(lexer.lex("3.1"))).value).toEqual(3.1);
  });

  it("Can Compile '34'", function() {
    expect(eval(compiler.compile(lexer.lex("34.2"))).value).toEqual(34.2);
  });
  
  it("should throw exception", function() {
    expect(function(){eval(compiler.compile(lexer.lex("3 + 4.1")))}).toThrow(new Error("Cannot perform op on unlike types: Operation May destabilize the runtime."));
  });

  it("can compile addition", function() {
    expect(eval(compiler.compile(lexer.lex("3.0 + 4.1"))).value).toEqual(7.1);
  });

  it("can compile subtraction", function() {
    expect(eval(compiler.compile(lexer.lex("3.0 - 4.2"))).value).toEqual(-1.2000000000000002);
  });

  it("can compile addition and subtraction", function() {
    expect(eval(compiler.compile(lexer.lex("3.0 + 4.0 - 5.1"))).value).toEqual(1.9000000000000004);
  });

  it("can compile multiplication", function() {
    expect(eval(compiler.compile(lexer.lex("3.0  * 6.1"))).value).toEqual(18.299999999999997);
  });

  it("can compile division", function() {
    expect(eval(compiler.compile(lexer.lex("6.0 / 3.2"))).value).toEqual(1.875);
  });

  it("can compile multiple operations", function() {
    expect(eval(compiler.compile(lexer.lex("8.0 + 6.0 / 2.4"))).value).toEqual(10.5);
  });


  it("can compile multiple operations parentheticals", function() {
    expect(eval(compiler.compile(lexer.lex("(8.0 + 6.0) / 2.5"))).value).toEqual(5.6);
  });

  it("can compile order of operation", function() {
    expect(eval(compiler.compile(lexer.lex("3.0 + 16.0 / (2.0  + 2.0) * 2.5"))).value).toEqual(13);
  });

  it("can compile order of operation", function() {
    expect(eval(compiler.compile(lexer.lex("3.0 + 16.0 / 2.0  + 2.0 * 2.4"))).value).toEqual(15.8);
  });

  it("can compile empty program", function() {
    expect(eval(compiler.compile(lexer.lex("\0")))).toEqual(null);
  });
});
