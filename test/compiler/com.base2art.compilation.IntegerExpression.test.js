describe("Integer Expression Compiler", function() {
  var compiler;
  var lexer;
  var insts = com.base2art.compilation.instructionSet;
  
  beforeEach(function() {
    lexer = new com.base2art.compilation.Lexer();
    compiler = new com.base2art.compilation.JavaScriptCompiler();
  });

  it("Can Compile '3'", function() {
    var af = compiler.compile(lexer.lex("3"));
    console.log(af);
    expect(eval(af).value).toEqual(3);
  });

  it("Can Compile '34'", function() {
    expect(eval(compiler.compile(lexer.lex("34"))).value).toEqual(34);
  });

  it("can compile addition", function() {
    expect(eval(compiler.compile(lexer.lex("3 + 4"))).value).toEqual(7);
  });

  it("can compile subtraction", function() {
    expect(eval(compiler.compile(lexer.lex("3 - 4"))).value).toEqual(-1);
  });

  it("can compile addition and subtraction", function() {
    expect(eval(compiler.compile(lexer.lex("3 + 4 - 5"))).value).toEqual(2);
  });

  it("can compile multiplication", function() {
    expect(eval(compiler.compile(lexer.lex("3  * 6"))).value).toEqual(18);
  });

  it("can compile division", function() {
    expect(eval(compiler.compile(lexer.lex("6 / 3"))).value).toEqual(2);
  });

  it("can compile integer division", function() {
    expect(eval(compiler.compile(lexer.lex("3 + 5 / 2"))).value).toEqual(5);
  });

  it("can compile multiple operations", function() {
    expect(eval(compiler.compile(lexer.lex("8 + 6 / 2"))).value).toEqual(11);
  });


  it("can compile multiple operations parentheticals", function() {
    expect(eval(compiler.compile(lexer.lex("(8 + 6) / 2"))).value).toEqual(7);
  });

  it("can compile order of operation", function() {
    expect(eval(compiler.compile(lexer.lex("3 + 16 / (2  + 2) * 2"))).value).toEqual(11);
  });

  it("can compile order of operation", function() {
    expect(eval(compiler.compile(lexer.lex("3 + 16 / 2  + 2 * 2"))).value).toEqual(15);
  });

  it("can compile empty program", function() {
    expect(eval(compiler.compile(lexer.lex("")))).toEqual(null);
  });
});
