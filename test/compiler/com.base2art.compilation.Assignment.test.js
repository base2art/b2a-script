describe("Integer Expression Compiler", function() {
  var compiler;
  var lexer;
  var insts = com.base2art.compilation.instructionSet;
  
  beforeEach(function() {
    lexer = new com.base2art.compilation.Lexer();
    compiler = new com.base2art.compilation.JavaScriptCompiler();
  });

  it("Can assignment and use", function() {
    var af = compiler.compile(lexer.lex("int a := 3\nint b:=4\nint c := a+ b"));
    console.log(af);
    expect(eval(af).value).toEqual(7);
  });
});
