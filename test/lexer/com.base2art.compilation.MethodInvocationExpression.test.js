describe("Method Invocation Lexer", function() {
  var lexer;
  var insts = com.base2art.compilation.instructionSet;
  
  beforeEach(function() {
    lexer = new com.base2art.compilation.Lexer();
    //console.log(lexer);
  });

  it("Can Compile '3.1'", function() {
    expect(lexer.lex("(3.1).toInt()")).toEqual([
      { inst : insts.ASSIGN_FLOAT, prams : { "NAME": "__1", "VALUE": "3.1" } },
      { inst : insts.CALL_METHOD, prams : { "object": "__1", "method_name": "toInt" } },
      { inst : insts.END }
    ]);
  });

  it("Can Compile '3.0'", function() {
    expect(lexer.lex("3.0")).toEqual([
      { inst : insts.ASSIGN_FLOAT, prams : { "NAME": "__1", "VALUE": "3.0" } },
      { inst : insts.END }
    ]);
  });
});
