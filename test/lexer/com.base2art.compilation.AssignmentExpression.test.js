describe("Assignment Lexer", function() {
  var lexer;
  var insts = com.base2art.compilation.instructionSet;
  
  beforeEach(function() {
    lexer = new com.base2art.compilation.Lexer();
  });

  it("Can Compile 'int a := 3'", function() {
    expect(lexer.lex("int a := 3")).toEqual([
      { inst : insts.DECLARE_INT, prams : { "NAME": "a" } },
      { inst : insts.LOAD_INT, prams : { "NAME": 1, "VALUE": "3" } },
      { inst : insts.VAR_TO_REF_INT, prams : { "NAME": "a", "VALUE": 1 } },
      { inst : insts.END }
    ]);
  });
  //~ it("Can assignment and use", function() {
    //~ expect(lexer.lex("int a := 3\nint b:=4\nint c := a+ b")).toEqual([
      //~ { inst : insts.DECLARE_INT, prams : { "NAME": "a" } },
      //~ { inst : insts.LOAD_INT, prams : { "NAME": 1, "VALUE": "3" } },
      //~ { inst : insts.VAR_TO_REF_INT, prams : { "NAME": "a", "VALUE": 1 } },
      //~ { inst : insts.END }
    //~ ]);
  //~ });
});
