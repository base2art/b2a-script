describe("Integer Lexer", function() {
  var lexer;
  var insts = com.base2art.compilation.instructionSet;
  
  beforeEach(function() {
    lexer = new com.base2art.compilation.Lexer();
    //console.log(lexer);
  });

  it("Can Compile '3'", function() {
    expect(lexer.lex("3")).toEqual([
      { inst : insts.LOAD_INT, prams : { "NAME": 1, "VALUE": "3" } },
      { inst : insts.END }
    ]);
  });

  it("Can Compile '34'", function() {
    expect(lexer.lex("34")).toEqual([
      { inst : insts.LOAD_INT, prams : { "NAME": 1, "VALUE": "34" } },
      { inst : insts.END }
    ]);
  });

  it("can compile addition", function() {
    expect(lexer.lex("3 + 4\0")).toEqual([
      { inst : insts.LOAD_INT, prams : { "NAME": 1, "VALUE": "3" } },
      { inst : insts.LOAD_INT, prams : { "NAME": 2, "VALUE": "4" } },
      { inst : insts.ADD, prams : { "REG" : 0, "AUGEND" : 1, "ADDEND" : 2 } },
      { inst : insts.REG_TO_VAR_INT, prams : { "NAME": 1, "REG" : 0 } },
      { inst : insts.END }
    ]);
  });

  it("can compile subtraction", function() {
    expect(lexer.lex("3 - 4\0")).toEqual([
      { inst : insts.LOAD_INT, prams : { "NAME": 1, "VALUE": "3" } },
      { inst : insts.LOAD_INT, prams : { "NAME": 2, "VALUE": "4" } },
      { inst : insts.SUB, prams : { "REG" : 0, "AUGEND" : 1, "ADDEND" : 2 } },
      { inst : insts.REG_TO_VAR_INT, prams : { "NAME": 1, "REG" : 0 } },
      { inst : insts.END }
    ]);
  });

  it("can compile addition and subtraction", function() {
    expect(lexer.lex("3 + 4 - 5\0")).toEqual([
      { inst : insts.LOAD_INT, prams : { "NAME": 1, "VALUE": "3" } },
      { inst : insts.LOAD_INT, prams : { "NAME": 2, "VALUE": "4" } },
      { inst : insts.ADD, prams : { "REG" : 0, "AUGEND" : 1, "ADDEND" : 2 } },
      { inst : insts.REG_TO_VAR_INT, prams : { "NAME": 1, "REG" : 0 } },
      { inst : insts.LOAD_INT, prams : { "NAME": 2, "VALUE": "5" } },
      { inst : insts.SUB, prams : { "REG" : 0, "AUGEND" : 1, "ADDEND" : 2 } },
      { inst : insts.REG_TO_VAR_INT, prams : { "NAME": 1, "REG" : 0 } },
      
      { inst : insts.END }
    ]);
  });

  it("can compile multiplication", function() {
    expect(lexer.lex("3 *    6\0")).toEqual([
      { inst : insts.LOAD_INT, prams : { "NAME": 1, "VALUE": "3" } },
      { inst : insts.LOAD_INT, prams : { "NAME": 2, "VALUE": "6" } },
      { inst : insts.MUL, prams : { "REG" : 0, "AUGEND" : 1, "ADDEND" : 2 } },
      { inst : insts.REG_TO_VAR_INT, prams : { "NAME": 1, "REG" : 0 } },
      { inst : insts.END }
    ]);
  });

  it("can compile division", function() {
    expect(lexer.lex("3 / 6\0")).toEqual([
      { inst : insts.LOAD_INT, prams : { "NAME": 1, "VALUE": "3" } },
      { inst : insts.LOAD_INT, prams : { "NAME": 2, "VALUE": "6" } },
      { inst : insts.DIV, prams : { "REG" : 0, "AUGEND" : 1, "ADDEND" : 2 } },
      { inst : insts.REG_TO_VAR_INT, prams : { "NAME": 1, "REG" : 0 } },
      { inst : insts.END }
    ]);
  });

  it("can compile arithmitec ", function() {
    expect(lexer.lex("4 + 6 / 3\0")).toEqual([
      { inst : insts.LOAD_INT, prams : { "NAME": 1, "VALUE": "4" } },
      { inst : insts.LOAD_INT, prams : { "NAME": 2, "VALUE": "6" } },
      { inst : insts.LOAD_INT, prams : { "NAME": 3, "VALUE": "3" } },
      { inst : insts.DIV, prams : { REG : 0, AUGEND : 2, ADDEND : 3 } },
      { inst : insts.REG_TO_VAR_INT, prams : { "NAME": 2, "REG" : 0 } },
      { inst : insts.ADD, prams : { REG : 0, AUGEND : 1, ADDEND : 2 } },
      { inst : insts.REG_TO_VAR_INT, prams : { "NAME": 1, "REG" : 0 } },
      { inst : insts.END }
    ]);
  });

  it("can compile empty program", function() {
    expect(lexer.lex("\0")).toEqual([{ "inst" : insts.END }]);
  });
});
