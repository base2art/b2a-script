

  this.programOp = function()
  {
    do 
    {
      this.exprOp();
      //this.assgnStmtOp();
    }
    while(false);//(this.look != '.' && this.position <= this.input.length);
    this.inst("END");
    
    
    //this.stmtsOp();
  }

  this.stmtsOp = function()
  {
    this.assgnStmtOp();
    this.stmtsRestOp();
  }

  this.stmtsRestOp = function()
  {  
    this.match(';');
    this.stmtsOp();
  }

  //~ this.stmtsRestOp = function()
  //~ {
    //~ /* EMPTY */
  //~ }

  this.assgnStmtOp = function()
  {
    console.log("assgnStmtOp");
    this.id();
    if (this.look == ':')
    {
      this.match(':');
    }
    
    this.exprOp();
  }

  this.exprOp = function()
  {
    console.log("exprOp" + this.look);
    
    if (this.isEnd())
    {
      return;
    }
    
    this.termOp();
    this.exprRestOp();
  }

  this.exprRestOp = function()
  {
    console.log("exprRestOp" + this.look);
    
    if (this.isEnd())
    {
      return;
    }
    
    this.skipWhiteSpace();
    
    // DO ADDITION
    var op = '';
    if (this.look == '+')
    {
      this.match('+');
      this.char();
      this.termOp();
      
      this.inst('ADD', { "REG": 0, "augend" : "__" + (this.var_counter - 1), "addend" : "__" + this.var_counter });
      this.inst('ASSIGN_REG', { "name": "__" + this.varCounter(), "REG" : 0 } );
    }
    
    if (this.look == '-')
    {
      this.match('-');
      this.char();
      this.termOp();
      
      this.inst('SUB', { "REG": 0, "augend" : "__" + (this.var_counter - 1), "addend" : "__" + this.var_counter });
      this.inst('ASSIGN_REG', { "name": "__" + this.varCounter(), "REG" : 0 } );
    }
    //~ this.match('-');
    
    this.exprRestOp();
  }

  this.termOp = function()
  {
    console.log("termOp" + this.look);
    
    if (this.isEnd())
    {
      return;
    }
    
    this.factorOp();
    //~ this.termRestOp();
  }

  this.termRestOp = function()
  {
    console.log("termRestOp" + this.look);
    
    if (this.isEnd())
    {
      return;
    }
    
    this.skipWhiteSpace();
    
    if (this.look == '*')
    {
      this.match('*');
      this.char();
      this.termOp();
      
      this.inst('MUL', { "REG": 0, "augend" : "__" + (this.var_counter - 1), "addend" : "__" + this.var_counter });
      this.inst('ASSIGN_REG', { "name": "__" + this.varCounter(), "REG" : 0 } );
    }
    
    if (this.look == '/')
    {
      this.match('/');
      this.char();
      this.termOp();
      
      this.inst('DIV', { "REG": 0, "augend" : "__" + (this.var_counter - 1), "addend" : "__" + this.var_counter });
      this.inst('ASSIGN_REG', { "name": "__" + this.varCounter(), "REG" : 0 } );
    }
    
    if (this.look == '%')
    {
      this.match('%');
      this.char();
      this.termOp();
      
      this.inst('MOD', { "REG": 0, "augend" : "__" + (this.var_counter - 1), "addend" : "__" + this.var_counter });
      this.inst('ASSIGN_REG', { "name": "__" + this.varCounter(), "REG" : 0 } );
    }
    
    //~ this.termRestOp();
  }

  //~ termRest: /* Rule 15 */
    //~ /* EMPTY */
    
  this.factorOp = function()
  {
    console.log("factorOp" + this.look);
    
    if (this.isEnd())
    {
      return;
    }
    
    this.skipWhiteSpace();
    
    if (this.look == '(')
    {
      this.match('(');
      this.exprOp();
      this.match(')');
    }
    else if (this.isDigit(this.look))
    {
      this.number();
      this.termOp();
    }
    else if (this.isLetter(this.look))
    {
      this.id();
    }
  }
  
  
  
  
  



  
//~ stmts
  //~ : assgnStmt
  //~ | assgnStmt ';' stmts
  //~ ;  
//~ assgnStmt
  //~ : ID ':='  expr
  //~ ;
//~ expr
  //~ : expr '+' term
  //~ | expr '-' term
  //~ | term
  //~ ;
//~ term
  //~ : term '*' factor
  //~ | term '/' factor
  //~ | term 'div' factor
  //~ | term 'mod' factor
  //~ | factor
  //~ ;
//~ factor
  //~ : '(' expr ')'
  //~ | ID
  //~ | NUM
  //~ ;


  
  //~ this.addOp = function()
  //~ {
   //~ this.match('+');
   //~ this.term();
   //~ this.inst({"op": "NEG", "term" : this.curr_term});
   //~ this.inst({"op": "ADD", "term" : this.curr_term});
  //~ }
  
  //~ this.subtractOp = function()
  //~ {
   //~ this.match('-');
   //~ this.term();
   //~ this.inst({"op": "ADD", "term" : this.curr_term});
  //~ }
  //~ 
  //~ this.termOP = function()
  //~ {
    //~ this.factor();
    //~ while (com.base2art.array.contains(['*', '/'], this.look))
    //~ {
      //~ //EmitLn('MOVE D0,-(SP)');
      //~ //this.inst({"op": "ADD", this.curr_term);
      //~ case this.look of
        //~ '*': Multiply;
        //~ '/': Divide;
      //~ else Expected('Mulop');
      //~ end;
    //~ }
  //~ }
