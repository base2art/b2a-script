
com.base2art.compilation["Lexer"] = function()
{
  
  this.match = function(ch, force, func)
  {
    if (this.look == ch)
    {
      this.char();
      func(this);
      return true;
    }
    
    if (force)
    {
      throw  ("No Match For '" + ch + "' == '" + this.look + "'");
    }
    
    return false;
  }
  
  this.word = function()
  {
    var id_text = "";
    if (this.isLetter(this.look))
    {
      id_text += this.look;
      this.char();
      
      while (this.isLetter(this.look) || this.isDigit(this.look))
      {
        id_text += this.look;
        this.char();
      }
    }
    
    return id_text;
  }
  
  this.type = function()
  {
    var word = this.word();
    
    if (word == "" || word == "string" || word == "int" || word == "float" || word == "bool")
    {
      return word;
    }
    
    if (word[0] == word[0].toUpperCase())
    {
      return word;
    }
    
    throw "LEXING ERROR: the value '" + word + "' doesn't follow the correct rules for a type";
  }
  
  this.id = function()
  {
    var word = this.word();
    
    if (word == "")
    {
      return word;
    }
    
    if (word[0] == word[0].toLowerCase())
    {
      return word;
    }
    
    throw "LEXING ERROR: the value '" + word + "' doesn't follow the correct rules for an id";
  }
  
  this.number = function()
  {
    var num = "";
    var isFloat = 0;
    while (this.isDigit(this.look))
    {
      num += this.look;
      this.char();
    }
    
    if (this.look == '.')
    {
      num += '.';
      this.char();
      isFloat = true;
    }
    
    while (this.isDigit(this.look))
    {
      num += this.look;
      this.char();
    }
    
    if (this.look == '.')
    {
      throw "Invalid Floating point number";
    }
    
    if (isFloat)
    {
      this.inst(this.insts.LOAD_FLOAT, { "NAME": this.varCounterUp(), "VALUE": num });
    }
    else
    {
      this.inst(this.insts.LOAD_INT, { "NAME": this.varCounterUp(), "VALUE": num });
    }
  }

  this.programOp = function()
  {
    this.stmtsOp();
    this.inst(this.insts.END);
  }

  this.stmtsOp = function()
  {
    this.assgnStmtOp();
    this.stmtsRestOp();
  }
  
  this.stmtsRestOp = function()
  {  
    this.match('\n', false, function(lexer){
      lexer.stmtsOp();
    });
    
  }
  
  this.typeDeclationOp = function(func)
  {
    var temp_type = this.type();
    this.skipWhiteSpace();
    var temp_id = this.id();
    
    if (temp_type != null && temp_type.length > 0 && temp_id != null && temp_id.length > 0)
    {
      if (temp_type == 'bool')
      {
        this.inst(this.insts.DECLARE_BOOLEAN, { "NAME": temp_id });
      }
      else if (temp_type == 'int')
      {
        this.inst(this.insts.DECLARE_INT, { "NAME": temp_id });
      }
      else if (temp_type == 'float')
      {
        this.inst(this.insts.DECLARE_FLOAT, { "NAME": temp_id });
      }
      else
      {
        this.inst(this.insts.DECLARE_REFERENCE, { "NAME": temp_id, "TYPE": temp_type });
      }
      
      func(this, temp_type, temp_id);
      // DECLARE TYPE;
      return true;
    }
    
    
    if (!(temp_type != null && temp_type.length > 0) ^ !(temp_id != null && temp_id.length > 0))
    {
      throw "Invalid Type Declaration '" +temp_type+ "' '" + temp_id + "'";
    }
    
    return false;
  }

  this.assgnStmtOp = function()
  {
    if (!this.typeDeclationOp(function(lexer, type, id){
        lexer.skipWhiteSpace();
        lexer.match(':', true, function(lexer){
          lexer.match('=', true, function(lexer){
            lexer.exprOp();
            // SWITCH
            lexer.inst(lexer.insts.VAR_TO_REF_INT, { "NAME": id, 'VALUE' : lexer.varCounterDown() });
          });
        })
      }))
    {
      this.exprOp();
    }
  }

  this.exprOp = function()
  {
    if (this.isEnd())
    {
      return;
    }
    
    this.termOp();
    this.exprRestOp();
  }

  this.exprRestOp = function()
  { 
    if (this.isEnd())
    {
      return;
    }
    
    this.skipWhiteSpace();
    
    var op = '';
    if (this.match('+', false, function(lexer){
      lexer.termOp();
      
      lexer.inst(lexer.insts.ADD, { "REG": 0, "ADDEND" : lexer.varCounterDown(), "AUGEND" : lexer.varCounterDown() });
      lexer.inst(lexer.insts.REG_TO_VAR_INT, { "NAME": lexer.varCounterUp(), "REG" : 0 } );
      
      lexer.exprRestOp();
    })) { return; }
    
    if (this.match('-', false, function(lexer){
      lexer.termOp();
      
      lexer.inst(lexer.insts.SUB, { "REG": 0, "ADDEND" : lexer.varCounterDown(), "AUGEND" : lexer.varCounterDown() });
      lexer.inst(lexer.insts.REG_TO_VAR_INT, { "NAME": lexer.varCounterUp(), "REG" : 0 } );
      
      lexer.exprRestOp();
    })) { return; }
  }

  this.termOp = function()
  {
    if (this.isEnd()) { return; }
    this.skipWhiteSpace();
    
    this.factorOp();
    this.termRestOp();
  }

  this.termRestOp = function()
  {
    if (this.isEnd()) { return; }
    this.skipWhiteSpace();
    
    if (this.match('*', false, function(lexer){
      lexer.factorOp();
      
      lexer.inst(lexer.insts.MUL, { "REG": 0, "ADDEND" : lexer.varCounterDown(), "AUGEND" : lexer.varCounterDown() });
      lexer.inst(lexer.insts.REG_TO_VAR_INT, { "NAME":  lexer.varCounterUp(), "REG" : 0 } );
      
      lexer.termRestOp();
    })) { return; }
    
    if (this.match('/', false, function(lexer){
      lexer.factorOp();
      
      lexer.inst(lexer.insts.DIV, { "REG": 0, "ADDEND" : lexer.varCounterDown(), "AUGEND" : lexer.varCounterDown() });
      lexer.inst(lexer.insts.REG_TO_VAR_INT, { "NAME": lexer.varCounterUp(), "REG" : 0 } );
      
      lexer.termRestOp();
    })) { return; }
    
    if (this.match('%', false, function(lexer){
      lexer.factorOp();
      
      lexer.inst(lexer.insts.MOD, { "REG": 0, "ADDEND" : lexer.varCounterDown(), "AUGEND" : lexer.varCounterDown() });
      lexer.inst(lexer.insts.REG_TO_VAR_INT, { "NAME": lexer.varCounterUp(), "REG" : 0 } );
      
      lexer.termRestOp();
    })) { return; }
  }
    
  this.factorOp = function()
  {
    if (this.isEnd())
    {
      return;
    }
    
    this.skipWhiteSpace();
    
    if (this.match('(', false, function(lexer){        
        lexer.exprOp();
        lexer.match(')', true, function(lexer){});
      })) { 
        return; 
        }
    
    if (this.isDigit(this.look))
    {
      this.number();
      return;
      //this.termOp();
    }
    else if (this.isLetter(this.look))
    {
      var my_id = this.id();
      
      this.inst(this.insts.LOAD_INT, { "NAME": this.varCounterUp(), "VALUE": my_id });
      return;
    }
  }
};

com.base2art.compilation.Lexer.prototype =  new com.base2art.compilation.LexerBase();
