
com.base2art.compilation["Lexer"] = function()
{
  
  this.insts = com.base2art.compilation.instructionSet;
  
  this.clear = function()
  {
    this.instructions = [];
    this.look = 0;
    this.position = -1;
    this.var_counter = 0;
  }
  
  this.varCounter = function(ch)
  {
    this.var_counter ++;
    return this.var_counter;
  }
  
  this.match = function(ch)
  {
    if (this.look == ch)
    {
      return;
    }
    
    throw  ("No Match For '" + ch + "' == '" + this.look + "'");
  }
  
  this.lex = function(text)
  {
    this.input = text;
    this.clear();
    this.char();
    
    this.programOp();
    
    return this.instructions;
  }
  
  this.isEnd = function()
  {
    if (this.look == '.')
    {
      return true;
    }
    
    if (this.position >= this.input.length)
    {
      return true;
    }
    
    return false;
  }
  
  this.char = function()
  {
    this.position ++;
    this.look = this.input.charAt(this.position);
  }
  
  this.inst = function(name, params)
  {
    this.instructions[this.instructions.length] = {
       "inst" : name, 
       "prams" : params
    };
  }
  
  this.id = function()
  {
    
  }
  
  this.number = function()
  {
    var num = "";
    while (this.isDigit(this.look))
    {
      num += this.look;
      this.char();
    }
    
    this.inst(this.insts.ASSIGN_VALUE, { "NAME": "__"  + this.varCounter(), "VALUE": num });
  }
  
  this.skipWhiteSpace = function()
  {
    while (this.look == ' ' || this.look == '\t')
    {
      this.char();
    }
  }
  
  this.isDigit = function(ch)
  {
    var myCharCode = ch.charCodeAt(0);
    if((myCharCode > 47) && (myCharCode <  58))
    {
       return true;
    }
    
    return false;
  }
  
  this.isLetter = function(ch)
  {
    var myCharCode = ch.charCodeAt(0);
    
    var smallA = 'a'.charCodeAt(0);
    var bigA = 'A'.charCodeAt(0);
    
    var smallZ = 'z'.charCodeAt(0);
    var bigZ = 'Z'.charCodeAt(0);
    
    if((smallA <= myCharCode && myCharCode <=  smallZ) || (bigA <= myCharCode && myCharCode <=  bigZ))
    {
       return true;
    }
    
    return false;
  }
  

  this.programOp = function()
  {
    do 
    {
      this.exprOp();
    }
    while(false);
    this.inst(this.insts.END);
    
    
    //this.stmtsOp();
  }

  this.stmtsOp = function()
  {
    this.assgnStmtOp();
    this.stmtsRestOp();
  }

  this.stmtsRestOp = function()
  {  
    this.match(';');
    this.stmtsOp();
  }

  //~ this.stmtsRestOp = function()
  //~ {
    //~ /* EMPTY */
  //~ }

  this.assgnStmtOp = function()
  {
    this.id();
    if (this.look == ':')
    {
      this.match(':');
    }
    
    this.exprOp();
  }

  this.exprOp = function()
  {
    if (this.isEnd())
    {
      return;
    }
    
    this.termOp();
    this.exprRestOp();
  }

  this.exprRestOp = function()
  {
    if (this.isEnd())
    {
      return;
    }
    
    this.skipWhiteSpace();
    
    // DO ADDITION
    var op = '';
    if (this.look == '+')
    {
      this.match('+');
      this.char();
      this.termOp();
      
      this.inst(this.insts.ADD, { "REG": 0, "AUGEND" : "__" + (this.var_counter - 1), "ADDEND" : "__" + this.var_counter });
      this.inst(this.insts.ASSIGN_REG, { "NAME": "__" + this.varCounter(), "REG" : 0 } );
      this.exprRestOp();
    }
    
    if (this.look == '-')
    {
      this.match('-');
      this.char();
      this.termOp();
      
      this.inst(this.insts.SUB, { "REG": 0, "AUGEND" : "__" + (this.var_counter - 1), "ADDEND" : "__" + this.var_counter });
      this.inst(this.insts.ASSIGN_REG, { "NAME": "__" + this.varCounter(), "REG" : 0 } );
      this.exprRestOp();
    }
  }

  this.termOp = function()
  {
    if (this.isEnd())
    {
      return;
    }
    
    this.factorOp();
    this.termRestOp();
  }

  this.termRestOp = function()
  {
    if (this.isEnd())
    {
      return;
    }
    
    this.skipWhiteSpace();
    
    if (this.look == '*')
    {
      this.match('*');
      this.char();
      this.factorOp();
      
      this.inst(this.insts.MUL, { "REG": 0, "AUGEND" : "__" + (this.var_counter - 1), "ADDEND" : "__" + this.var_counter });
      this.inst(this.insts.ASSIGN_REG, { "NAME": "__" + this.varCounter(), "REG" : 0 } );
      this.termRestOp();
    }
    
    if (this.look == '/')
    {
      this.match('/');
      this.char();
      this.factorOp();
      
      this.inst(this.insts.DIV, { "REG": 0, "AUGEND" : "__" + (this.var_counter - 1), "ADDEND" : "__" + this.var_counter });
      this.inst(this.insts.ASSIGN_REG, { "NAME": "__" + this.varCounter(), "REG" : 0 } );
      this.termRestOp();
    }
    
    if (this.look == '%')
    {
      this.match('%');
      this.char();
      this.factorOp();
      
      this.inst(this.insts.MOD, { "REG": 0, "AUGEND" : "__" + (this.var_counter - 1), "ADDEND" : "__" + this.var_counter });
      this.inst(this.insts.ASSIGN_REG, { "NAME": "__" + this.varCounter(), "REG" : 0 } );
      this.termRestOp();
    }
  }
    
  this.factorOp = function()
  {
    if (this.isEnd())
    {
      return;
    }
    
    this.skipWhiteSpace();
    
    if (this.look == '(')
    {
      this.match('(');
      this.exprOp();
      this.match(')');
    }
    else if (this.isDigit(this.look))
    {
      this.number();
    }
    else if (this.isLetter(this.look))
    {
      this.id();
    }
  }
};
