

com.base2art.compilation["CompilerBase"] = function()
{
  this.insts = com.base2art.compilation.instructionSet;
  
  this.compile = function(instructions)
  {
    var i = 0;
    this.startup();
    
    for (i = 0; i < instructions.length; i++)
    {
      this.compileInstruction(instructions[i]);
    }
    
    var complete = this.complete();
    return complete;
  }
  
  this.compileInstruction = function(instruction)
  {
    var name = instruction["inst"];
    var prams = instruction["prams"];
    
    switch(name)
    {
      case null:
        throw "unknown command '" + name + "'";
      case undefined:
        throw "unknown command '" + name + "'";
      case this.insts.END:
        this.end();
        break;
        
          
      case this.insts.ADD:
        this.add(prams["REG"], prams["AUGEND"], prams["ADDEND"]);
        break;
      case this.insts.SUB:
        this.sub(prams["REG"], prams["AUGEND"], prams["ADDEND"]);
        break;
      case this.insts.MUL:
        this.mul(prams["REG"], prams["AUGEND"], prams["ADDEND"]);
        break;
      case this.insts.DIV:
        this.div(prams["REG"], prams["AUGEND"], prams["ADDEND"]);
        break;
      case this.insts.MOD:
        this.mod(prams["REG"], prams["AUGEND"], prams["ADDEND"]);
        break;
        
        
      case this.insts.DECLARE_BOOLEAN:
        this.declare_boolean(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.DECLARE_INT:
        this.declare_int(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.DECLARE_FLOAT:
        this.declare_float(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.DECLARE_ARRAY:
        this.declare_array(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.DECLARE_REFERENCE:
        this.declare_reference(prams["NAME"], prams["VALUE"]);
        break;
        
      
      case this.insts.LOAD_BOOLEAN:
        this.load_boolean(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.LOAD_INT:
        this.load_int(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.LOAD_FLOAT:
        this.load_float(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.LOAD_ARRAY:
        this.load_array(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.LOAD_REFERENCE:
        this.load_reference(prams["NAME"], prams["VALUE"]);
        break;
      
      
      case this.insts.REG_TO_VAR_BOOLEAN:
        this.reg_to_var_boolean(prams["NAME"], prams["REG"]);
        break;
      case this.insts.REG_TO_VAR_INT:
        this.reg_to_var_int(prams["NAME"], prams["REG"]);
        break;
      case this.insts.REG_TO_VAR_FLOAT:
        this.reg_to_var_float(prams["NAME"], prams["REG"]);
        break;
      case this.insts.REG_TO_VAR_ARRAY:
        this.reg_to_var_array(prams["NAME"], prams["REG"]);
        break;
      case this.insts.REG_TO_VAR_REFERENCE:
        this.reg_to_var_reference(prams["NAME"], prams["REG"]);
        break;
      
      
      
      
      
      
      
      case this.insts.VAR_TO_REF_BOOLEAN:
        this.var_to_ref_boolean(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.VAR_TO_REF_INT:
        this.var_to_ref_int(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.VAR_TO_REF_FLOAT:
        this.var_to_ref_float(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.VAR_TO_REF_ARRAY:
        this.var_to_ref_array(prams["NAME"], prams["VALUE"]);
        break;
      case this.insts.VAR_TO_REF_REFERENCE:
        this.var_to_ref_reference(prams["NAME"], prams["VALUE"]);
        break;
        
      
        
        
      default:
        throw "unknown command '" + name + "'";
    }
  }
}
/*
END : 1,
    
    ASSIGN_REG : 2,
    
    ASSIGN_BOOLEAN: 9,
    ASSIGN_INT : 10,
    ASSIGN_FLOAT : 11,
    ASSIGN_ARRAY : 12,
    ASSIGN_REFERENCE : 13,
    
    
    ADD : 16,
    SUB : 17,
    MUL : 18,
    DIV : 19,
    MOD : 20
    
    */
