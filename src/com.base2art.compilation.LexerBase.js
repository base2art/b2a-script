com.base2art.compilation["LexerBase"] = function()
{
  this.insts = com.base2art.compilation.instructionSet;
  this.var_counter_array = [];
  this.var_counter = 0;
  
  this.clear = function()
  {
    this.instructions = [];
    this.look = 0;
    this.position = -1;
    this.var_counter_array = [];
  }
  
  this.varCounterUp = function()
  {
    var value = this.var_counter_array.length +1;
    this.var_counter_array.push(value);
    return value;
  }
  
  this.varCounterDown = function()
  {
    return this.var_counter_array.pop();
  }
  
  this.lex = function(text)
  {
    this.input = text;
    this.clear();
    this.char();
    
    this.programOp();
    
    return this.instructions;
  }
  
  this.isEnd = function()
  {
    if (this.look == '\0')
    {
      return true;
    }
    
    if (this.position >= this.input.length)
    {
      return true;
    }
    
    return false;
  }
  
  this.char = function()
  {
    this.position ++;
    this.look = this.input.charAt(this.position);
  }
  
  this.inst = function(name, params)
  {
    this.instructions[this.instructions.length] = {
       "inst" : name, 
       "prams" : params
    };
  }
  
  this.skipWhiteSpace = function()
  {
    while (this.look == ' ' || this.look == '\t')
    {
      this.char();
    }
  }
  
  this.isDigit = function(ch)
  {
    var myCharCode = ch.charCodeAt(0);
    if((myCharCode > 47) && (myCharCode <  58))
    {
       return true;
    }
    
    return false;
  }
  
  this.isLetter = function(ch)
  {
    var myCharCode = ch.charCodeAt(0);
    
    var smallA = 'a'.charCodeAt(0);
    var bigA = 'A'.charCodeAt(0);
    
    var smallZ = 'z'.charCodeAt(0);
    var bigZ = 'Z'.charCodeAt(0);
    
    if((smallA <= myCharCode && myCharCode <=  smallZ) || (bigA <= myCharCode && myCharCode <=  bigZ))
    {
       return true;
    }
    
    return false;
  }
}
