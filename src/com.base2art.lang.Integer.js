
com.base2art["lang"] = com.base2art.lang || {
}

com.base2art.lang["Integer"] = com.base2art.lang.Integer || function(value)
{
  this.value = value;
  
  // FLAWED IMPLEMENTATION
  // SUFFERS FROM THE SAME PROBLEM JAVA DOES
  // ASSEMBLY VERSIONING SHOULD BE INCLUDED...
  // AHH SHIT.
  // FIX BEFORE GO LIVE
  this.__typeData__ = function()
  {
    return { name:"com.base2art.lang.Integer", instance: this};
  }
  
  this.__add__ = function (item)
  {
    this.verifySameType(item);
    return new com.base2art.lang.Integer(this.value + item.value);
  }
  
  this.__sub__ = function (item)
  {
    this.verifySameType(item);
    return new com.base2art.lang.Integer(this.value - item.value);
  }
  
  this.__mul__ = function (item)
  {
    this.verifySameType(item);
    return new com.base2art.lang.Integer(this.value * item.value);
  }
  
  this.__div__ = function (item)
  {
    this.verifySameType(item);
    return new com.base2art.lang.Integer(Math.floor(this.value / item.value));
  }
  
  this.__mod__ = function (item)
  {
    this.verifySameType(item);
    return new com.base2art.lang.Integer(this.value % item.value);
  }
  
  this.toFloat = function ()
  {
    this.verifySameType(item);
    return new com.base2art.lang.Float(this.value + 0.0);
  }
  
  this.toInt = function()
  {
    return this;
  }
  
  this.toString = function()
  {
    return this.value;
  }
}

com.base2art.lang.Integer.prototype = new com.base2art.lang.Object();
