
com.base2art.compilation["JavaScriptCompiler"] = function()
{
  this._native = []
  
  this.native = function(inst)
  {
    this._native[this._native.length] = inst;
  }
  
  this.nativeStmt = function(inst)
  {
    this._native[this._native.length] = inst + ";";
  }
  
  this.startup = function()
  {
    // com.base2art.compilation.registers || 
    this.native("var $b2ar$ = [];");
  }
  
  this.end = function()
  {
    //console.log("End of program...");
  }
  
  
  this.declare_any = function(name) { this.nativeStmt("var " + name); };
  this.declare_boolean = this.declare_any;
  this.declare_int = this.declare_any;
  this.declare_float = this.declare_any;
  this.declare_array = this.declare_any;
  this.declare_reference = this.declare_any;
  
  this.load_type = function(name, value, type)
  {
     this.native("__" + name + " = new " + type + "(" + value + ");");
  }
  
  this.load_boolean = function(name, value) { this.load_type(name, value, "com.base2art.lang.Boolean"); };
  this.load_int = function(name, value) { this.load_type(name, value, "com.base2art.lang.Integer"); };
  this.load_float = function(name, value) { this.load_type(name, value, "com.base2art.lang.Float"); };
  this.load_array = function(name, value) { this.load_type(name, value, "com.base2art.lang.Array"); };
  this.load_refernce = function(name, value, type) { this.load_type(name, value, type); };
  
  
  
  this.reg_to_var_int = function(name, reg)
  {
    this.native("__" + name + " = $b2ar$[" + reg + "];");
  }
  
  this.var_to_ref_int = function(name, value)
  {
    this.native(name + " = __" + value + ";");
  }
  
  //~ this.assign_int = function(name, value)
  //~ {
    //~ this.native(name + " = new com.base2art.lang.Integer(" + value + ");");
  //~ }
  
  //~ this.declare_int = function(name)
  //~ {
    //~ this.native("var " + name + ";");
  //~ }
  //~ 
  //~ this.store_int = function(name, value)
  //~ {
    //~ this.native(name + " = " + value + ";");
  //~ }
  //~ 
  //~ this.assign_float = function(name, value)
  //~ {
    //~ this.native(name + " = new com.base2art.lang.Float(" + value + ");");
  //~ }
  
  
  
  
  
  
  
  
  
  this.add = function(reg, augend, added)
  {
    this.native("$b2ar$[" + reg + "] = " + ("__" + augend  + ".__add__(__" + added) + ");");
  }
  
  this.sub = function(reg, augend, added)
  {
    this.native("$b2ar$[" + reg + "] = " + ("__" + augend  + ".__sub__(__" + added) + ");");
  }
  
  this.mul = function(reg, augend, added)
  {
    this.native("$b2ar$[" + reg + "] = " + ("__" + augend  + ".__mul__(__" + added) + ");");
  }
  
  this.div = function(reg, augend, added)
  {
    this.native("$b2ar$[" + reg + "] = " + ("__" + augend  + ".__div__(__" + added) + ");");
  }
  
  this.mod = function(reg, augend, added)
  {
    this.native("$b2ar$[" + reg + "] = " + ("__" + augend  + ".__mod__(__" + added) + ");");
  }
  
  
  
  
  this.complete = function()
  {
    return this._native.join("\n");
  }
}

com.base2art.compilation.JavaScriptCompiler.prototype = new com.base2art.compilation.CompilerBase();
