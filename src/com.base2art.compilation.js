


//document.write('<script type="text/javascript" src="com.base2art.core.js"><\/script>');

com.base2art["compilation"] = com.base2art.compilation || {
  instructionSet : {
    
    END                   : 1,
    
    ADD                   : 10,
    SUB                   : 11,
    MUL                   : 12,
    DIV                   : 13,
    MOD                   : 14,
    
    
    // DECLARE REFERENCE
    DECLARE_BOOLEAN       : 20,
    DECLARE_INT           : 21,
    DECLARE_FLOAT         : 22,
    DECLARE_ARRAY         : 23,
    DECLARE_REFERENCE     : 24,
    
    // CONST TO VARIABLE
    LOAD_BOOLEAN          : 25,
    LOAD_INT              : 26,
    LOAD_FLOAT            : 27,
    LOAD_ARRAY            : 28,
    LOAD_REFERENCE        : 29,
    
    REG_TO_VAR_BOOLEAN    : 30,
    REG_TO_VAR_INT        : 31,
    REG_TO_VAR_FLOAT      : 32,
    REG_TO_VAR_ARRAY      : 33,
    REG_TO_VAR_REFERENCE  : 34,
    
    VAR_TO_REG_BOOLEAN    : 35,
    VAR_TO_REG_INT        : 36,
    VAR_TO_REG_FLOAT      : 37,
    VAR_TO_REG_ARRAY      : 38,
    VAR_TO_REG_REFERENCE  : 39,
    
    REF_TO_VAR_BOOLEAN    : 40,
    REF_TO_VAR_INT        : 41,
    REF_TO_VAR_FLOAT      : 42,
    REF_TO_VAR_ARRAY      : 43,
    REF_TO_VAR_REFERENCE  : 44,
    
    VAR_TO_REF_BOOLEAN    : 45,
    VAR_TO_REF_INT        : 46,
    VAR_TO_REF_FLOAT      : 47,
    VAR_TO_REF_ARRAY      : 48,
    VAR_TO_REF_REFERENCE  : 49,
    
    ASSIGN_REG            : 126,
    
    BAD_ABORT             : 127
    
  },
  
  registers : []
}



/*
function com_base2art_extend(parent, name, child)
{
  parent[name] = child;
  if (!name in parent)
  {
    parent
  }
}

com = var test = com_base2art_extend(window, "com", {});


if (com == null || typeof(com) != "object")
{
  
}

com = {
  base2art : {
    compiler : 
  }
}


*/
