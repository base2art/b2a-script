
var com = com || {};
com["base2art"] = com.base2art || {}

com.base2art["array"] = {
  
  contains : function (a, obj)
  {
    var i = a.length;
    while (i--) 
    {
      if (a[i] === obj)
      {
         return true;
      }
    }
    
    return false;
  }
}
