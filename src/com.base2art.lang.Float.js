
com.base2art["lang"] = com.base2art.lang || {
}
com.base2art.lang["Float"] =  com.base2art.lang.Float || function(value)
{
  this.value = value;
  
  this.__typeData__ = function()
  {
    return { name:"com.base2art.lang.Float", instance: this};
  }
  
  this.__add__ = function (item)
  {
    this.verifySameType(item);
    return new com.base2art.lang.Float(this.value + item.value);
  }
  
  this.__sub__ = function (item)
  {
    this.verifySameType(item);
    return new com.base2art.lang.Float(this.value - item.value);
  }
  
  this.__mul__ = function (item)
  {
    this.verifySameType(item);
    return new com.base2art.lang.Float(this.value * item.value);
  }
  
  this.__div__ = function (item)
  {
    this.verifySameType(item);
    return new com.base2art.lang.Float(this.value / item.value);
  }
  
  this.__mod__ = function (item)
  {
    this.verifySameType(item);
    return new com.base2art.lang.Float(this.value % item.value);
  }
  
  this.toFloat = function()
  {
    return this.value;
  }
  
  this.toInt = function()
  {
    return new com.base2art.lang.Integer(parseInt(this.value));
  }
  
  this.toString = function()
  {
    return this.value;
  }
}

com.base2art.lang.Float.prototype = new com.base2art.lang.Object();
