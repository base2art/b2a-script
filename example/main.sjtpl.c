#include <com.base2art.lang.Integer>
#include <com.base2art.lang.Float>
#include <com.base2art.lang.String>


struct:
  
  void returnNullMethod1:
    pass
    return Null.Value
    
  void returnNullMethod2:
    pass
    return
  
  Integer returnIntegerMethod:
    pass
    return 3
  
  void methodTakesParamester1:
    String x, String y, Integer z:
    
    String b := x
    if b = a:
      printf()
      return
    else if (b == c).not:
      printf()
      return
    else:
      printf("", 1, 2)
    return
    
    
  
  void methodTakesParamester2:
    String x, 
    String y,
    Integer z:
      pass
      return 4
  

