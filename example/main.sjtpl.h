
#include <com.base2art.lang.Integer>
#include <com.base2art.lang.Float>
#include <com.base2art.lang.String>

@Extends(TItem)
@Implements(IItem)
struct:

  void returnNullMethod1;

  void returnNullMethod2;
    
  Integer returnIntegerMethod;
  
  void methodTakesParamester1: String x, String y, Integer z;
  
  void methodTakesParamester2: String x, String y, Integer z;

